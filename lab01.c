/*
	Archivo: lab01.c

	Autores: Felipe Parra Castañeda, Fernando García Polgatti

	Descripción: 
				Programa utiliza la librería mpi para la multiplicación de N matrices de manera distribuida, en este caso
				se utiliza el método "Granja" en el que un proceso maestro asigna a los K procesos esclavos las tareas a realizar.

				El programa lee un archivo de texto el cual contiene el número de veces que se multiplicará matrices y las dimensiones
				de las matrices a multiplicar. como los valores de los datos de cada matriz es irrelevante, solo se utilizarán
				las dimensiones de éstas.
 
	Compilación: mpicc lab01.c -o lab01

		(Para una correcta ejecución se debe compilar y ejecutar el archivo getmemory.c)

	Ejecución  : mpirun -np K ./lab01 < datos.txt

				K         = cantidad de Procesadores en que se dividirán las operaciones.
				datos.txt = es generado por el programa anterior

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include "mpi.h"
#include <string.h>

// Se define FARMMASTER para diferenciar el proceso Maestro.
#define FARMMASTER 0

struct info{
	long estado;
	long tamanoA;
	long tamanoB;
	long tamanoC;
};

// Variables globales (no implica que sean globales para todos los procesos, cada proceso tiene sus variables globales)
int whoami, hm_are_we;
MPI_Status status;

//Declaración de funciones
void Usage(char *message);
void multiplicarMatrices(unsigned char **matrixA, unsigned char **matrixB, unsigned char **RM, long a, long b,long c);
int  estadosProc(int *S, int length);
void process(int mode, int esclavo, int n);

int main(int argc, char* argv[])
{

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int  me, n, esclavo, mode, boton = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &hm_are_we);
	MPI_Comm_rank(MPI_COMM_WORLD, &whoami);
	MPI_Get_processor_name(processor_name, &me);

	// Con este If se separa el proceso Maestro de los esclavos
	if(whoami == FARMMASTER)
	{
		if(argc!= 1)
		{
			Usage(argv[0]);
			MPI_Finalize();
		}
		else
		{
			// Se obtiene de la primera línea del archivo ingresado, la cantidad de 
			// multiplicaciones que se realizarán, 
			scanf("%d", &n);
			esclavo = hm_are_we - 1;
			process(mode,esclavo,n);
			MPI_Finalize();
		}
	}
	else if(whoami != FARMMASTER && argc == 1)
	{
		process(mode,esclavo,n);
		MPI_Finalize();
	}
}

void Usage(char *message)
{
	printf("Cómo usar:\n");
	printf("\tEn la terminal: mpirun -np N %s -O < datafile\n\n", message);
	printf("\tN: Cluster Size\n\n");
	printf("\tdatafile: \"archivo.txt\" el cual es generado\n\tpor el programa \"getMemory.c\"\n\n");
	printf("\t_____________________________________________\n\n");

	fflush(stdout);
	MPI_Finalize();
}

void multiplicarMatrices(unsigned char **matrixA, unsigned char **matrixB, unsigned char **matrixC, long a, long b,long c)
{
	long n, y = 0, w = 0;
	for(long i = 0; i < a; i++)
	{
		for(long m = 0; m < c; m++)
		{
			n = 0;
			for(long j = 0; j < b; j++)
			{
				matrixC[w][y] = matrixC[w][y] + matrixA[i][j]*matrixB[n][m];
				n = n + 1;
			}
			y = y + 1;
			if(y == c)
			{
				y = 0;
				w = w + 1;
			}
		}
	}
}

int estadosProc(int *estadoProcesos, int largoArray)
{
	//chequear si todos los procesos están desocupados o no.
	for(int i = 0; i < largoArray; i++)
	{
		if(estadoProcesos[i] == 1)
			return 0;
	}
	return 1;
}

void process(int mode, int esclavo, int n)
{
	//separa el trabajo a los esclavos
	int      esperandoEstadoTerminado, total, i, k, *estadoEsclavo;
	//Mensaje que se les envía a los esclavos
	struct   info informacion;
	clock_t  tiempoGlobalStart, tiempoGlobalEnd;
	double   global_cpu_time_used;


	//separa la tarea del proceso maestro que derivará el trabajo al resto de procesos
	if(whoami == FARMMASTER)
	{
		estadoEsclavo = calloc(esclavo, sizeof(int));

		esperandoEstadoTerminado = 1;
		informacion.estado       = 2;
		total                    = n;
		tiempoGlobalStart        = clock();

		while(esperandoEstadoTerminado == 1)
		{
			if(n == 0)
			{
				while (estadosProc(estadoEsclavo, esclavo) == 0)
				{
					//Esta función bloquea el proceso hasta que se reciba un mensaje con las características especificadas. 
					//en este caso, $i donde se almacena lo recibido, 1: numero de elementos maximos a recibir
					//MPI_INIT tipo de dato que recibe
					//4: -> MPI_ANY_TAG, etiqueta 
					//MPI_COMM_WORLD: comunicador por el que se recibe.
					//&status: estructura informativa del estado.
					MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, 4, MPI_COMM_WORLD, &status);
					estadoEsclavo[i-1] = 0;
				}
				//y 2 si el proceso está ocupado.
				//Se ocupará el 3 para representar que el proceso está libre.
				informacion.estado = 3;
				esperandoEstadoTerminado = 0;
				for(i = 1; i <= esclavo; ++i)
				{
					MPI_Send(&informacion, 5, MPI_LONG, i, 4, MPI_COMM_WORLD);
				}
			}
			else
			{
				if (esclavo < n)
				{
					k = esclavo;
				}
				else{
					k = n;
				}
				for(i = 1; i <= k; ++i)
				{
					//maestro manda k-1 tareas a los k-1 esclavos
					// envia n tareas a los n esclavos
					scanf("%ld %ld %ld", &informacion.tamanoA, &informacion.tamanoB, &informacion.tamanoC);
					MPI_Send(&informacion, 5, MPI_LONG, i, 4, MPI_COMM_WORLD);//envia los datos de la matriz al esclavo 1
					estadoEsclavo[i - 1] = 1;
					n = n - 1;
				}
				while( n > 0)
				{
					//espera para que los esclavos manden su confirmación luego obtiene el id y lo envía a una nueva tarea

					MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, 4, MPI_COMM_WORLD, &status);
					scanf("%ld %ld %ld", &informacion.tamanoA, &informacion.tamanoB, &informacion.tamanoC);
					MPI_Send(&informacion, 5, MPI_LONG, i, 4, MPI_COMM_WORLD);
					n = n - 1;
				}
			}
		}

		tiempoGlobalEnd = clock();
		global_cpu_time_used  = (float)(tiempoGlobalEnd - tiempoGlobalStart) / CLOCKS_PER_SEC;

		printf("\n\nTiempo global de computación: %f", global_cpu_time_used);
		fflush(stdout);

		printf("\nNúmero total de pares de matrices multiplicadas: %d\n", total);
		fflush(stdout);
	}
	else{
		esperandoEstadoTerminado        = 1;
		unsigned char                  **matrixA, **matrixB, **matrixC;
		int                            cont = 0; 
		clock_t tiempoProcesoStart, tiempoProcesoEnd;
		double proceso_cpu_time_used;

		tiempoProcesoStart = clock();

		while(esperandoEstadoTerminado == 1)
		{
			//Se reciben los datos enviados por el FARMMASTER a través del struct
			MPI_Recv(&informacion,5,MPI_LONG,FARMMASTER,4,MPI_COMM_WORLD,&status);

			if(informacion.estado == 2)
			{
				cont = cont+1;
				matrixA = calloc(informacion.tamanoA, sizeof(unsigned char*));
				for(long j = 0; j < informacion.tamanoA; j ++ )
				{
					matrixA[j] = calloc(informacion.tamanoB, sizeof(unsigned char));
				}

				matrixB = calloc(informacion.tamanoB, sizeof(unsigned char*));
				for(long j = 0; j < informacion.tamanoB; j ++ )
				{
					matrixB[j] = calloc(informacion.tamanoC, sizeof(unsigned char));
				}

				matrixC = calloc(informacion.tamanoA, sizeof(unsigned char*));
				for(long j = 0; j < informacion.tamanoA; j ++ )
				{
					matrixC[j] = calloc(informacion.tamanoC, sizeof(unsigned char));
				}
				tiempoProcesoStart = clock();

				//Multiplicación de matrices.
				multiplicarMatrices(matrixA,matrixB,matrixC, informacion.tamanoA, informacion.tamanoB, informacion.tamanoC);
				
				//&whoami = referencia al vecto de elementos a enviar
				//1 tamaño del vector
				//tipo de dato que se envía
				//pid del proceso destino, FARMMASTER = 0
				//4 es la etiqueta
				//MPI_COMM_WORLD comunicador por el que se manda
				MPI_Send(&whoami, 1 , MPI_INT, FARMMASTER,4,MPI_COMM_WORLD);
				free(matrixA);
				free(matrixB);
				free(matrixC);
			}
			else
			{
				tiempoProcesoEnd = clock();
				proceso_cpu_time_used = (float)(tiempoProcesoEnd - tiempoProcesoStart) / CLOCKS_PER_SEC;
				printf("\n Esclavo[%3d]:\n\tNúmero total de pares de matrices multiplicadas: %d.\n\tTiempo de computación empleado multiplicando: %f[secs].\n", whoami, cont, proceso_cpu_time_used);
				fflush(stdout);
				esperandoEstadoTerminado = 0;
			}
		}
	}
}








