#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include "mpi.h"
#include <string.h>

#define SILENT -1
#define VERBOSE 1

#define BUSY 2
#define FREE 3

#define TAG 4
#define MASTER_NODE 0

int whoami, hm_are_we;
MPI_Status status;

/*
*
*
*/

void Usage(char *message)
{
	printf("asdf");
	printf("In terminal: mpirun -np N %s -O < datafile\n\n", message);
	printf(" O in {S,V}\n\n");
	printf(" N: Cluster Size\n\n");
	printf(" S: Execution on silent Mode. \n\n");
	printf(" V: Execution on Verbose Mode \n\n");
	printf("**********\n\n");

	fflush(stdout);
	MPI_Finalize();
}

void MatrixMul(unsigned char **M1, unsigned char **M2, unsigned char **RM, long a, long b,long c)
{
	/*
	funcion recibe 3 matrices y sus dimensiones 
	m1 y m2 son las matrices que se multiplicarán y la tercera es la que guardará el resultado
	MR es la dimensión axc, será a*c? 
	*/
	long n, y = 0, w = 0;

	for(long i = 0; i < a; i++)
	{
		for(long m = 0; m < c; m++)
		{
			n = 0;
			for(long j = 0; j < b; j++)
			{
				RM[w][y] = RM[w][y] + M1[i][j]*M2[n][m];
				n = n + 1;
			}
			y = y + 1;
			if(y == c)
			{
				y = 0;
				w = w + 1;
			}
		}
	}
}

int everyonefree(int *S, int length)
{
	/*
		function that recives an array and an integer
		el arreglo es el estado de trabajo de todos los esclavos, que será ...
		y el entero es el largo del arreglo
		la funcion retornada 1 si todos los esclavos terminaron, de lo contrario
		retornará 0.
	*/
	for(int i = 0; i < length; i++)
	{
		if(S[i] == 1)
			return 0;
	}
	return 1;
}

int min(int x, int y)
{
	/*
		recibe 2 enteros y retorna el mínimo
	*/
	if (x<y)
	{
		return x;
	}
	
	return y;
}

void resetMatrix(unsigned char **Matrix)
{
	// liberar memoria
	if(Matrix)
	{
		free(*Matrix);
		*Matrix = NULL;
	}
}

void process(int mode, int slaves, int n)
{
	//separa el trabajo a los esclavos
	int notyet, total, i, k, *slaveStatus;
	long dim[5]; //message to send to the slaves, from the master
	float E_cpu;
	clock_t cs, ce;
	long E_wall;
	time_t ts, te;

	if(whoami == MASTER_NODE)
	{
		slaveStatus = calloc(slaves, sizeof(int));

		notyet = 1;
		dim[0] = BUSY;
		dim[4] = mode;
		total = n;

		printf("\nStarting ... \n\n");
		fflush(stdout);

		// START of distributed execution
		ts = time(NULL);
		cs = clock();

		while(notyet == 1)
		{
			printf("primer While\n");
			fflush(stdout);
			if(n == 0)
			{
				while (everyonefree(slaveStatus, slaves) == 0)
				{
					printf("segundo While\n");
					fflush(stdout);
					MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
					slaveStatus[i-1] = 0;
				}
				dim[0] = FREE;
				notyet = 0;
				for(i = 1; i <= slaves; ++i)
				{
					MPI_Send(&dim, 5, MPI_LONG, i, TAG, MPI_COMM_WORLD);
				}
			}
			else
			{
				for(i = 1, k = min(slaves,n); i <= k; ++i)
				{
					//maestro manda k-1 tareas a los k-1 esclavos
					// envia n tareas a los n esclavos
					scanf("%ld %ld %ld", &dim[1], &dim[2], &dim[3]);
					MPI_Send(&dim, 5, MPI_LONG, i, TAG, MPI_COMM_WORLD);//envia los datos de la matriz al esclavo 1
					slaveStatus[i - 1] = 1;
					n = n - 1;
				}
				while( n > 0)
				{
					printf("tercer While\n");
					fflush(stdout);
					//espera para que los esclavos manden su confirmación luego obtiene el id y lo envía a una nueva tarea
					printf("AAA");
					fflush(stdout);
					
					MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

					if ( dim[4] == VERBOSE)
					{
						printf("\n\n (MASTER) received From Slave[%d]. Confirmation: sendint new task..\n",status.MPI_SOURCE);
						fflush(stdout);
					}

					scanf("%ld %ld %ld", &dim[1], &dim[2], &dim[3]);
					MPI_Send(&dim, 5, MPI_LONG, i, TAG, MPI_COMM_WORLD);
					n = n - 1;
				}
			}
		}
		// end of distributed exectuion
		ce = clock();
		te = time(NULL);
		E_wall = (long)(te - ts);
		E_cpu = (float)(ce - cs) / CLOCKS_PER_SEC;
		//T_cpu = T_cpu + E_cpu;
		//T_wall = T_wall + E_wall;

		printf("\n\nMaster elapse cpu time: %f [secs] Elapsed wall time: %ld", E_cpu, E_wall);
		fflush(stdout);

		printf("\nTotal number of matrix pairs: %d\n", total);
		fflush(stdout);
	}
	else{
		notyet = 1;
		unsigned char **M1, **M2, **RM;
		int cont = 0; float E_cpu;
		float T_cpu = 0;
		long T_wall = 0;

		while(notyet == 1)
		{
			MPI_Recv(&dim,5,MPI_LONG,MASTER_NODE,TAG,MPI_COMM_WORLD,&status);

			if(dim[4] == VERBOSE)
			{
				printf("\n\n (SLAVE %3d) Data received from master. Data: a(%ld), b(%ld) & c(%ld)\n", whoami, dim[1], dim[2], dim[3]);
				fflush(stdout);
			}
			if(dim[0] == BUSY)
			{
				cont = cont+1;
				if(dim[4] == VERBOSE)
				{
					printf("\n (SLAVE %3d) GENERATING: M1(%ldx%ld), M2(%ldx%ld) & RM(%ldx%ld) \n", whoami, dim[1], dim[2], dim[2], dim[3], dim[1],dim[3]);
					fflush(stdout);
				}
				M1 = calloc(dim[1], sizeof(unsigned char*));
				for(long j = 0; j < dim[1]; j ++ )
				{
					M1[j] = calloc(dim[2], sizeof(unsigned char));
				}

				M2 = calloc(dim[2], sizeof(unsigned char*));
				for(long j = 0; j < dim[2]; j ++ )
				{
					M2[j] = calloc(dim[3], sizeof(unsigned char));
				}

				RM = calloc(dim[1], sizeof(unsigned char*));
				for(long j = 0; j < dim[1]; j ++ )
				{
					RM[j] = calloc(dim[3], sizeof(unsigned char));
				}
				ts = time(NULL);
				cs = clock();

				if(dim[4] == VERBOSE)
				{
					printf("\n (SLAVE %3d) TASK %d. elapse cpu time: %f[secs], Elapsed wall time %ld[secs]\n", whoami, cont,E_cpu,E_wall);
					fflush(stdout);
				}

				MatrixMul(M1,M2,RM, dim[1], dim[2], dim[3]);

				ce = clock();
				te = time(NULL);
				E_wall = (long)(te - ts);
				E_cpu = (float)(ce - cs) / CLOCKS_PER_SEC;
				T_cpu = T_cpu + E_cpu;
				T_wall = T_wall + E_wall;

				printf("\nSLAVE[%3d] TASK %d. Elapsed CPU Time: %f[secs] Elapsed wall time: %ld[Secs]\n",whoami,cont,E_cpu,E_wall);
				fflush(stdout);

				MPI_Send(&whoami, 1 , MPI_INT, MASTER_NODE,TAG,MPI_COMM_WORLD);



				resetMatrix(M1);
				resetMatrix(M2);
				resetMatrix(RM);
			}
			else
			{
				printf("\n SLaVE[%3d]: done... \n\t number of matrix pairs worked: %f\n\tAverage cpu time: %f[secs], Average wall time: %ld[secs]\n", whoami, E_cpu, E_cpu, E_wall);
				fflush(stdout);
				notyet = 0;
			}
		}
	}
}


int main(int argc, char* argv[])
{
	char processor_name[MPI_MAX_PROCESSOR_NAME];

	int me, n, slaves, mode;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &hm_are_we);
	MPI_Comm_rank(MPI_COMM_WORLD, &whoami);
	MPI_Get_processor_name(processor_name, &me);

	printf("Process [%d] Alive on %s\n", whoami, processor_name);
	fflush(stdout);

	if(whoami == MASTER_NODE)
	{
		if(argc!= 2)
		{
			Usage(argv[0]);
		}
		else
		{
			if (strcmp(argv[1], "-V") == 0)
			{
				mode = VERBOSE;
			}
			else
			{
				if(strcmp(argv[1], "-S") == 0)
					mode = SILENT;
				else
					Usage(argv[0]);
			}
			scanf("%d", &n);
			slaves = hm_are_we - 1;
		}
	}
	process(mode,slaves,n);
	MPI_Finalize();
}








