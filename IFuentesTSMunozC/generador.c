/*
generador.c: genera data.txt compuesto por:
	N
	f1 c11 c12
	f2 c21 c22
	.
	.
	.
	fi ci1 c2i
	.
	.
	.
	fn cn1 c2n
Donde:
	N = Cantidad de matrices a definir
	f1  = N° Filas Matriz 1
	ci1 = N° Columnas Matriz 1 = N° Filas Matriz 2
	c2i = N° Columnas Matriz 2 

Autores:	Sergio Muñoz Campos - Ignacio Fuentes Terán
Correo:		sergio.munoz.ca@usach.cl - ifuentes3854@gmail.com

Santiago de Chile, 07/11/2017
*/

#include <stdlib.h> 
#include <stdio.h>
#include <time.h>
#include <string.h>

int random_int(int min, int max);
int getMemory();
int parseLine(char* line);

int main(int argc, char *argv[]) {
	FILE *pfile;
	int i, N, f, c1, c2;
	int memoriaDisponible = getMemory();
	int cantMatrices = atoi(argv[1]);
	int cantProcesos = atoi(argv[2]);
	int tamanoMatrizA, tamanoMatrizB, tamanoMatrizC;

	int tamanoMaximoMatriz = memoriaDisponible / cantProcesos;

	pfile = fopen("datos.txt", "w");
	if(!pfile){
		printf("No se puede crear archivo data.txt!\n");
	  	exit(0);
	}

	printf("\nCantidad de matrices que se crearán: %d", cantMatrices);
	printf("\nMemoria disponible: %d\n", memoriaDisponible);
	printf("Por lo tanto, como las multiplicaciones las harán %d procesos, cada uno tiene \n%d espacio para crear 3 matrices.\n", cantProcesos, tamanoMaximoMatriz);
	printf("");
	srand(time(NULL));
	fprintf(pfile, "%d\n", cantMatrices);
	for(i=0; i<cantMatrices - 1; i++){
		f = random_int(1, 1000);
		c1 = random_int(1, 1000);
		while ( f * c1 > tamanoMaximoMatriz )
		{
			f = random_int(1, 1000);
			c1 = random_int(1, 1000);
		}
		tamanoMatrizA = f * c1;
		c2 = random_int(1, 1000);
		tamanoMatrizB = c1 * c2;
		tamanoMatrizC = f * c2;

		while (tamanoMatrizA + tamanoMatrizB + tamanoMatrizC >= memoriaDisponible)
		{
			c2 = random_int(1, 1000);
			tamanoMatrizB = c1 * c2;
			tamanoMatrizC = f * c2;
		}

		fprintf(pfile, "%d %d %d\n", f, c1, c2);
	}
	fclose(pfile);
	return 0;
	
}

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getMemory()
{
	FILE* file = fopen("/proc/meminfo", "r");
    int result = -1;
    char line[128];
    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "MemFree:", 8) == 0){
            result = parseLine(line);
            break;
        }
    }
    return result;
}

int random_int(int min, int max)
{
   return min + rand() % (max+1 - min);
}
