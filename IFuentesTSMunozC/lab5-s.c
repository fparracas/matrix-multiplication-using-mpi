/*
lab5-s.c: lee datos.txt compuesto por:
	N
	f1 c11 c12
	f2 c21 c22
	.
	.
	.
	fi ci1 c2i
	.
	.
	.
	fn cn1 c2n
Donde:
	N = Cantidad de matrices a definir
	f1  = N° Filas Matriz 1
	ci1 = N° Columnas Matriz 1 = N° Filas Matriz 2
	c2i = N° Columnas Matriz 2 
Y realiza las N multiplicaciones de matrices de los ordenes leídos
respectivamente, en K procesos especificados en:
mpirun -np K ./lab5-s.c < datos.txt

Autores:	Sergio Muñoz Campos - Ignacio Fuentes Terán
Correo:		sergio.munoz.ca@usach.cl - ifuentes3854@gmail.com

Santiago de Chile, 07/11/2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

MPI_Status status;
int **matriz1, **matriz2, **matriz3;

int main(int argc, char **argv) {
	int numtasks, myid, numworkers, source, dest, rows, offset;
	int i, j, k, f, c2, c1, n, flag=0;	
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	scanf("%d", &n);
	numworkers = numtasks-1;
	printf( "Comienza\n");
	printf( "Cantidad de procesos: %d\n", numtasks);
	printf( "ID Actual: %d\n", myid);

	if (myid == 0) {
		printf( "Dentro\n");	
		rows = n/numworkers;
		offset = 0;
		for (dest=1; dest<=numworkers; dest++){
			printf( "ID actual: %d\n", dest);	
			scanf("%d %d %d", &f,&c1,&c2);				
			printf("%d %d %d \n", f,c1,c2);
			MPI_Send(&offset, 1, MPI_INT, dest, 1, MPI_COMM_WORLD);
			MPI_Send(&rows, 1, MPI_INT, dest, 1, MPI_COMM_WORLD);
			MPI_Send(&f, 1, MPI_INT, dest, 1, MPI_COMM_WORLD);
			MPI_Send(&c1, 1, MPI_INT, dest, 1, MPI_COMM_WORLD);
			MPI_Send(&c2, 1, MPI_INT, dest, 1, MPI_COMM_WORLD);
			offset = offset + rows;
		}

		for (i=1; i<=numworkers; i++){
			source = i;
			MPI_Recv(&offset, 1, MPI_INT, source, 2, MPI_COMM_WORLD, &status);
			MPI_Recv(&rows, 1, MPI_INT, source, 2, MPI_COMM_WORLD, &status);
			printf( "El proceso %d de %d ha finalizado.\n", offset, numtasks);
		}
	}

	if (myid > 0) {
		source = 0;
		MPI_Recv(&offset, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&rows, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&f, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&c1, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&c2, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
		printf("ID: %d, Matriz 1: %dx%d Matriz 2: %dx%d \n", myid, f, c1, c1, c2);
		matriz1 = (int **)calloc(f, sizeof(int *));
		matriz2 = (int **)calloc(c1, sizeof(int *));
		matriz3 = (int **)calloc(f, sizeof(int *));
		for (i=0; i<f; i++) 
			matriz1[i] = (int *) calloc(c1, sizeof(int));
		for (i=0; i<c1; i++) 
			matriz2[i] = (int *) calloc(c2, sizeof(int));
		for (i=0; i<f; i++) 
			matriz3[i] = (int *) calloc(c2, sizeof(int));

		for(i=0; i<f; ++i){
        	for(j=0; j<c2; ++j){
            	for(k=0; k<c1; ++k){
                	matriz3[i][j]=matriz3[i][j] + (matriz1[i][k]*matriz2[k][j]);
            	}
			}
    	}
    	free(matriz1);
		free(matriz2);
		free(matriz3);
		flag++;
		MPI_Send(&offset, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
		MPI_Send(&rows, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);		
	}

  	MPI_Finalize();
  	return 0;  	
}


